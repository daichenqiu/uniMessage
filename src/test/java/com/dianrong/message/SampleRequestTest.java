package com.dianrong.message;

import org.junit.Test;

import com.dianrong.util.CodingUtils;
import com.dianrong.util.HttpUtil;

public class SampleRequestTest {

    @Test
    public void sampleTest() throws Exception{
        StringBuffer params = null;

        params = new StringBuffer();
        params.append("sender=").append(CodingUtils.encodeBase64URL("silei"));

        String httpPath = "http://localhost:8888/uniMessage/api/sample/getSampleUserByUserId?userId=120";
        String httpPathSMS = "http://localhost:8888/uniMessage/api/sample/sendSMS";

        String result = HttpUtil.sendPostRequestByParam(httpPathSMS,
                params.toString());
    }
}

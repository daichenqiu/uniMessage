package com.dianrong.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Ignore;
import org.junit.Test;

import com.dianrong.util.HttpUtil;

public class SampleControllerTest {
	private final String url = "http://localhost:8888/uniMessage/api";

	@Ignore
	public void testSample() throws Exception{
		String params = "userId=120";
		String httpPath = url + "/sample/getSampleUserByUserId";
		String result = HttpUtil.sendPostRequestByParam(httpPath,
				params);
		System.out.println(result);
	}
	
	@Test
	@SuppressWarnings({"rawtypes","unused"})
	public void testSendingMap() throws Exception{
		HttpClient httpClient = new DefaultHttpClient();
		try{
			String serverURL = url + "/sample/getSampleUserByMap";
			HttpPost httpPost = new HttpPost(serverURL);
			// String param = "userId=120";

			Map param = new HashMap();
			param.put("userId", "120");

			List<NameValuePair> formparams = new ArrayList<NameValuePair>();
			if (param != null) {
				Set set = param.keySet();
				Iterator iterator = set.iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					Object value = param.get(key);
					formparams.add(new BasicNameValuePair(key.toString(), value
							.toString()));
				}
			}
			UrlEncodedFormEntity urlEntity = new UrlEncodedFormEntity(formparams,
					"UTF-8");
			httpPost.setEntity(urlEntity);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			
		}finally{
			httpClient.getConnectionManager().shutdown(); 
		}
		
		
	}
	
	

//	还可以用map作为参数
//	  List<NameValuePair> formparams = new ArrayList<NameValuePair>(); 
//	  if(param!=null){
//	  Set set = param.keySet(); 
//	   Iterator iterator = set.iterator();
//	   while (iterator.hasNext()) {
//	    Object key = iterator.next();
//	    Object value = param.get(key);
//	    formparams.add(new BasicNameValuePair(key.toString(), value.toString())); 
//	  }
//	  }
}

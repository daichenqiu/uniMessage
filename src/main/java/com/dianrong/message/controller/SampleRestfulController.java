package com.dianrong.message.controller;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dianrong.message.pojo.SampleUser;
import com.dianrong.message.pojo.json.JsonResult;
import com.dianrong.message.service.SampleService;

@RestController
@RequestMapping("/sample")
public class SampleRestfulController {

	@Autowired
	private SampleService sampleService;
	
	private static final Logger logger = LoggerFactory.getLogger(SampleRestfulController.class);
	
	@RequestMapping(value="/getSampleName",method=RequestMethod.GET)
    public String getSampleName(@RequestParam(value="name") String name){
        return name;
    }
     
    @RequestMapping("getSampleUser")
    public SampleUser getSampleUser(@RequestParam("userName") String userName,String userId,String email){
        SampleUser sampleUser = new SampleUser();
        sampleUser.setUserId(userId);
        sampleUser.setName(userName);
        sampleUser.setEmail(email);;
        return sampleUser;
    }
    @SuppressWarnings({"unused"})
    @RequestMapping("getSampleUserByUserId")
    public JsonResult getSampleUserByUserId(@RequestParam("userId") String userId){
    	
    	SampleUser phoneCardInfo = null;
        phoneCardInfo = sampleService.getSampleUserByUserId(userId);
        
        return JsonResult.success(phoneCardInfo);
    }

    @RequestMapping("sendSMS")
    public JsonResult sendSMS(@RequestParam("sender") String sender){
    	
    	String result = null;
    	result = sampleService.sendTest(sender);
        
        return JsonResult.success(result);
    }
    @SuppressWarnings({"unused"})
    @RequestMapping("getSampleUserByMap")
    public JsonResult getSampleUserByMap(HttpServletRequest request){
    	HttpServletRequest req = request;
    	req.getParameterMap();
    	SampleUser phoneCardInfo = null;
    	System.out.println(req);
        
        return JsonResult.success(phoneCardInfo);
    }

}

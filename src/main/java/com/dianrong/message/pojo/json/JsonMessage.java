package com.dianrong.message.pojo.json;

import java.util.LinkedList;
import java.util.List;


public class JsonMessage {

    private String brief;
    private List<JsonError> errors = new LinkedList<JsonError>();

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public List<JsonError> getErrors() {
        return errors;
    }

    public void setErrors(List<JsonError> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "JsonMessage{" +
            "brief='" + brief + '\'' +
            ", errors=" + errors +
            '}';
    }
}

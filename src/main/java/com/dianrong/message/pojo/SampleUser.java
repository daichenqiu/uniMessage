package com.dianrong.message.pojo;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SampleUser {
    
    private String userId;
    private String name;
    private String email;
    private Date createDate;
     
    public SampleUser(){
    	super();
    }
    
    public SampleUser(Map<Object, Object> result) throws Exception {
        this.userId = result.get("ID").toString();
        this.name = (String) result.get("NAME");
        this.email = (String) result.get("EMAIL");
    }
    
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
   
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
     
     

}

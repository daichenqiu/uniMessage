//package com.dianrong.message.msengine;
//
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.util.StringUtils;
//
///**
// * Created by Silei Cheng on 2015/9/8.
// */
//public class MSEngine {
//    protected static final Logger logger = LoggerFactory.getLogger(MSEngine.class);
//
//    private final static String LENDERSALES_PRODUCTID = "201412053";
//    private final static String CRMS_PRODUCTID = "201581353";
//
//    public static SLMessage sendSMS(String phoneNumber, String recipient, Map<String, String> vars,SLSMSTemplate smsTemplate ) {
//        return sendTemplatedSMS( CRMS_PRODUCTID,phoneNumber, recipient, vars, smsTemplate, SLBaseMessageTransport.QueuePolicy.IMMEDIATE);
//    }
//
//    public static SLMessage sendTemplatedSMS(String productid, String phoneNumber,String recipient, Map<String, String> vars, SLSMSTemplate smsTemplate, SLBaseMessageTransport.QueuePolicy queuePolicy){
//
//        // Build message
//        SLMessage msg = buildTemplatedSMSService(phoneNumber,recipient, vars, smsTemplate);
//        msg.setProductid(productid);
//        // Send it via sms
//        return sl.messaging.SLMessageEngine.sendMessage(msg, TLMsg.Transport.SMS_TIANLI, queuePolicy);
//    }
//
//    protected static SLMessage buildTemplatedSMSService(String phoneNumber,String recipient, Map<String, String> vars, SLSMSTemplate smsTemplate){
//
//        SLMessage msg = new SLMessage();
//        msg.setMessage(smsTemplate.buildMessageContent(vars));
//        msg.setMessageType(smsTemplate.getType());
//        msg.setNotificationType(smsTemplate.getNotificationType());
//        //msg.setRecipient(recipient);
//        msg.setVars(vars);
//
//        if(StringUtils.isEmpty(phoneNumber)){
//            logger.warn("Actor #" + " passing a null mobile phone. Not sending SMS");
//        }else{
//            msg.setToAddress(phoneNumber);
//        }
//
//        return msg;
//    }
//}

//package com.dianrong.message.query;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//
//import lc.ui.request.RequestUtils;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Repository;
//
//import sl.commonservice.query.SLBaseQuery;
//
//import com.dianrong.message.pojo.SampleUser;
//
//@Repository
//public class SampleQuery   extends SLBaseQuery {
//
//
//    private final Logger logger = LoggerFactory.getLogger(SLBaseQuery.class);
//    public enum ParamName {
//    	id,
//    }
//    public SampleQuery() {
//
//    	setArgValue("id", null);
//        
//        sortByColumns.put("id", "id");
//        sortByColumns.put("name", "name");
//        sortByColumns.put("email", "email");
//    }
//    
//    public void prepareQuery(){
//    	selectColumns = " distinct id id,name name,email email ";
//        selectFrom = " sl$actor "
//    	+ " where id = 120 ";
//        
//        
//    }
//    public void readParams(HttpServletRequest request) {
//        super.readParams(request);
//        
//        String id = request.getParameter(ParamName.id.toString());
//        if (!RequestUtils.isEmpty(id)) {
//            setArgValue(ParamName.id.name(), id);
//        }
//    }
//	
//    @SuppressWarnings({"rawtypes","unchecked"})
//    public SampleUser getUser() {
//        List<Map> results = execute();
//        List<SampleUser> users = new ArrayList<SampleUser>();
//        for (Map row : results) {
//            try {
//            	users.add(new SampleUser(row));
//            } catch (Exception e) {
//                logger.warn("Execute SLCrmsCardInfoQuery Result Error, error: {}", e);
//            }
//        }
//        if(users != null&&users.size()>0)
//        	return users.get(0);
//        else {
//			return new SampleUser();
//		}
//    }
//	
//}

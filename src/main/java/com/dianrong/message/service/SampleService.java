package com.dianrong.message.service;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dianrong.message.pojo.SampleUser;
import com.dianrong.util.ResultCode;

@Service
public class SampleService {

//	@Autowired
//    private SampleQuery query;
	
	public SampleUser getSampleUserByUserId(String userId){
		SampleUser user = new SampleUser();
		user.setUserId(userId);
		user.setName("name");
		return user;
	}
	
	public String sendSms(String sender, ArrayList<String> receiverPhoneNumber, String smsTemplate, Map<String, String> contextVars){
		
//	    SLSMSTemplate template = SLSMSTemplate.valueOf(smsTemplate);
//
//	    Map<String, String> vars = new HashMap<String, String>();
//	    
//	    SLMessage message =
//	        MSEngine.sendSMS(receiverPhoneNumber.get(0), sender, vars, template);
//	    if (message != null && message.getStatus() == Status.SENT) {
//	      return ResultCode.SUCCESS;
//	    }
//
//	    return ResultCode.ERROR;
		return ResultCode.ERROR;
	}
//	
	public String sendTest(String sender) {
		//String sender = "silei";
		ArrayList<String> receiverPhoneNumber = new ArrayList<String>();
		String smsTemplate = "WorkflowLenderSalesNews";
		Map<String, String> contextVars = null;
		
		receiverPhoneNumber.add("18521309382");
		
		return sendSms(sender,receiverPhoneNumber,smsTemplate,contextVars);
	}
}

package com.dianrong.util;

import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CodingUtils {
    private static final Logger logger = LoggerFactory.getLogger(CodingUtils.class);
	
	private static final Base64 base64 = new Base64();
	
	public static String encodeBase64URL(String arg) throws Exception{		
		String content = new String(Base64.encodeBase64(URLEncoder.encode(arg,"UTF-8").getBytes()));
		return content;
	}
	
	public static void main(String[] args)
	{
		String str="杨先生/小姐，您好！感谢您的信任，您已充值成功，点融网为您安排了专属顾问服务，如果以后有问题请联系我们或加“订阅号”，联系电话：021-61532183【点融网】";
		try {
			logger.info("{} : [encodeBase64URL : {}] ",encodeBase64URL(str),encodeBase64URL(str));
			String input = encodeBase64URL(str);
			String output= decodeBase64URL(input);
			logger.info("{} : [output : {}] ",output,output);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String decodeBase64URL(String arg)throws Exception{
		String content = URLDecoder.decode(new String(base64.decode(arg.getBytes())),"UTF-8");
		return content;
	}
	
	public static String encodeURL(String arg) throws Exception{		
		String content = URLEncoder.encode(arg,"UTF-8");
		return content;
	}
	
	public static String decodeURL(String arg)throws Exception{
		String content = URLDecoder.decode(arg,"UTF-8");
		return content;
	}
	
}

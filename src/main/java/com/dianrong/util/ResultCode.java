package com.dianrong.util;

/**
 * Result codes that will be returned by our action classes.
 * @author Tom
 *
 */
public final class ResultCode {

    /* main result codes */
    public static final String HITPROTECTION = "hitProtection";   
    public static final String NO_COOKIES = "noCookies";         
    public static final String SUCCESS = "success";         // successful completion
    public static final String INPUT = "input";             // error during validation of input parameters
    public static final String ERROR = "error";             // error during execution
    public static final String MAXPOSTNUMBER = "max";             // error during execution
    public static final String RESET_PWD= "resetPwd";
    public static final String VALID = "valid";
    public static final String INVALID = "invalid"; 
    public static final String LOGIN = "login";		// login required
    public static final String WORKFLOW_LOGIN = "workflow_login";		// workflow login required
    public static final String AUTHENTICATED = "authenticated";	// user not logged in
    public static final String WORKFLOW_AUTHENTICATED = "workflow_authenticated";	// user not logged in
    public static final String UNAUTHENTICATED = "unauthenticated";	// user not logged in
    public static final String SESSION_TIMEOUT = "session_timeout";	// session timed out
    public static final String WORKFLOW_SESSION_TIMEOUT = "workflow_session_timeout";	// session timed out

    public static final String MUST_BE_LENDER = "mustbelender";
    public static final String MUST_BE_BORROWER = "mustbeborrower"; 
    public static final String MUST_BE_TRADER = "mustbetrader"; 
    public static final String MUST_BE_ADMIN = "mustbeadmin";    

    public static final String FAILED = "failed";
	public static final String UNCONFIRMED_MEMBER = "unconfirmed_member";
	public static final String CONFIRMED_MEMBER = "confirmed_member";
	public static final String UNCONFIRMED_BORROWER = "unconfirmed_borrower";
	public static final String UNCONFIRMED_LENDER = "unconfirmed_lender";
	
    public static final String MISSING_PRECONDITION = "missing_precondition";
	public static final String MISSING_SESSION_DATA = "missing_session_data";
	
	public static final String INVITE_FRIENDS = "invitefriends";
	public static final String INPUT_LENDER = "input_lender";            
	
	public static final String NOT_AUTHORIZED_TO_BUY = "not_authorized_to_buy";  
	
    /* borrower registration results */
    public static final String PREQUALIFIED = "prequalified";
    public static final String BORROWER_ALREADY_REGISTERED = "borrower_already_registered";
    public static final String LOAN_ALREADY_ACCEPTED = "loan_already_accepted";
    
    /* lender registration results */
    public static final String LENDER_ALREADY_REGISTERED = "lender_already_registered";  
    public static final String LENDER_NOT_A_MEMBER = "lender_not_a_member";
    public static final String LENDER_SSN_MAX_REACHED = "lenderssnMax";  
    
    /* portfolio building results */
    public static final String UNDIVERSIFIED = "undiversified";
    public static final String ADD_ERROR = "adderror";
    public static final String NO_CURRENT_PORTFOLIO = "no_current_portfolio";
    public static final String INSUFFICIENT_FUNDS = "insufficientfunds";
    public static final String INSUFFICIENT_LOAN_INVENTORY = "insufficientloaninventory";
    public static final String ACTOR_ALREADY_HAS_AN_OPEN_PORTFOLIO = "actor_already_has_an_open_portfolio";
    public static final String START_NEW_PORTFOLIO_ACCEPT = "continue";
    public static final String START_NEW_PORTFOLIO_REJECT= "cancel";
    public static final String INCOMPLETE = "incomplete";
    public static final String ORDER_EXISTS = "orderexists";

    // account display results
    public static final String LENDER = "lender";
    public static final String BORROWER = "borrower";
    public static final String TRADER = "trader";
    public static final String TRADER_UNCONFIRMED = "traderUnconfirmed";
    public static final String TRADER_REJECTED = "traderRejected";    
    public static final String NOFUNDINGSOURCE = "nofundingsource";
    public static final String UNVERIFIEDFUNDINGSOURCE = "unverifiedfundingsource";
    public static final String INFUNDING_LOAN = "infunding_loan";

	public static final String REDIRECT = "redirect";
	
	//QUIET Period
	public static final String QUIET_PERIOD = "quiet_period";
	
	/* Lender SEC Qualification */
	public static final String MUST_QUALIFY = "must_qualify";	
	public static final String MUST_QUALIFY_BROWSE = "must_qualify_browse";
     
    /* new cosigner registration result*/
    public static final String COSIGNER_PERSONAL_INFO = "cosigner_personal_info";
    public static final String THIRD_BIND_ACCOUNT = "third_bind_account";
    public static final String THIRD_BIND_SUCCESS = "third_bind_success";
    public static final String THIRD_ALREADY_BIND = "third_already_bind";
    public static final String PAYMENT_QUERY = "payment_query";
    public static final String PAYMENT_PENDDING = "payment_pendding";
    public static final String NOT_AUTHORIZE="not authorize";
    
    // Ajax result codes
    public static final String AJAX_SUCCESS = "ajaxSuccess";
    public static final String AJAX_ERROR = "ajaxError";
    // loan type
    public static final String PERSONAL_LOAN="personal_loan";
    public static final String BUSINESS_LOAN="business_loan";
    public static final String SMALL_LOAN="small_loan";
    public static final String SELECT_LOAN = "select_loan";
    public static final String MCA_LOAN = "mca_loan";
    
    public static final String FOREIGN_LENDER="foreign_lender";
    
    public static final String SELECT_USERNAME = "select_username";
    public static final String BLACKLISTED_ID = "blacklisted_id";
    public static final String BLACKLISTED_SSN = "blacklisted_ssn";

    //privileges based features
    public static final String UNAUTHORIZED = "notAllowed";
    public static final String UNAUTHORIZED_ASYN = "notAllowed_asyn";
    
    //Services
    public static final String SERVICE_STATUS_DISABLED= "service_disabled";
    
    public static final String LOAN_EXIST = "loan_exist";
    public static final String FIELDS_INCOMPLETE = "incomplete";
    
    //third party auth
    public static final String INVALID_ACTOR_KEY = "invalid_actor_key";
    public static final String INVALID_LOAN_KEY = "invalid_loan_key";
}


